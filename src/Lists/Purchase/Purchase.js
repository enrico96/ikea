import React, {Component} from 'react';
import * as Products from '../Products.json';



class Purchase extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            items: []
        }
    }


    handleDeletion = (id, a, code) => {
        this.props.handleDeletion(id, code);
    }

    render()
    {
        if (this.props.items.length <= 0) return <p>La lista de compra está vacía.</p>
        else
        {

            return (
                <table className="table table-striped table-sm table-responsive-md table-responsive-sm">
                    <thead className="thead-dark">
                        <tr>
                        <th scope="col" >Num Id</th>
                        <th scope="col" >Nombre</th>
                        <th scope="col" >tamaño</th>
                        <th scope="col" >Precio €</th>
                        <th scope="col" >Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.items.map( (item, id) => 
                            <Product key={id} itemId={item}  product={Products.list[item]} ifProductDeleted={this.handleDeletion.bind(this, id)}/>
                        )}
                    </tbody>
                </table>
            );
        }
    }
}

class Product extends Component
{

    deleteProduct = () => {
        this.props.ifProductDeleted(this.props.itemId, this.props.product.id);
    }

    render()
    {

        return (
            <tr id={`item-id-${this.props.itemId}`}>
                <td>{this.props.product.id}</td>
                <td>{this.props.product.name}</td>
                <td>{`${this.props.product.tam.x}x${this.props.product.tam.y}x${this.props.product.tam.z}`} cm</td>
                <td>{this.props.product.price} €</td>
                <td className="removePannel"> <button onClick={this.deleteProduct.bind(this)} className="btn btn-sm btn-warning">Eliminar</button> </td>
            </tr>
        )
    }
}


export default Purchase;