import React, {Component} from 'react';



class Search extends Component
{

    handleSearch = (e) => {
        e.preventDefault();
        alert('éste botón es inútil');
    }

    updateSearch = (e) => {
        this.props.handleSearch(e.target.value);
    }

    render()
    {
        return (
        <form>
            <div className="form-row align-items-center">
                <div className="col-sm-12 my-1">
                    <label className="sr-only" htmlFor="inlineFormInputGroupSearch">Nombre</label>
                    <div className="input-group">
                        <div className="input-group-prepend">
                        <div className="input-group-text">Buscador</div>
                        </div>
                        <input 
                        onChange={this.updateSearch.bind(this)} 
                        value={this.props.value}
                        type="text" className="form-control" id="inlineFormInputGroupSearch" placeholder="Nombre, Id o tamaño" />
                        <button className="btn btn-primary" onClick={this.handleSearch.bind(this)}>Buscar</button>
                    </div>
                </div>
            </div>
        </form>
        );
    }
}


export default Search;