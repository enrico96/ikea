import React, {Component, Fragment} from 'react';
import './Catalog.css';

import * as Products from '../Products.json';


class Catalog extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            products: Products.list,
            other: "other"
        }

        // this.makeList = this.makeList.bind(this); // Legacy
    }
    render()
    {
        return (
            // <Fragment>{this.makeList(Products.list.length)}</Fragment>  // Legacy
            <Fragment>
                {this.state.products.map((item,key) => 
                    <Product 
                    key={key} 
                    keycode={key} 
                    similares={this.searchSim.bind(this)}
                    itemToAdd={this.addItemToList.bind(this)}
                    product={item}
                    filter={this.props.filter}
                    // No conseguí actualizar de nuevo los disponibles
                    updateAmount={this.props.updateCode}
                    />
                )}
            </Fragment>
        )
    }

    searchSim = (value) => {
        this.props.handleSearchSim(value);
    }

    addItemToList = (e, v) => {
        this.props.handleParentFunction(e, v);   
    }

    /*
    * Constructor de objetos. Inserto un JSON con todos los objetos, y creo los objetos 
    * necesarios en base de dicho array // El cual terminé quitando por optimizar el código
    */
    // makeList(amount)
    // {
    //     let output = [];
    //     for (let i = 0; i < amount; i++)
    //     {
    //         output.push(<Product 
    //             key={i} 
    //             keycode={i} 
    //             similares={this.searchSim.bind(this)}
    //             itemToAdd={this.addItemToList.bind(this)}
    //             product={Products.list[i]}
    //             filter={this.props.filter}
    //             />);
    //     }
    //     return output;
    // }

}

class Product extends Component
{

    constructor(props)
    {
        super(props);
        this.state = {
            display: false,
            disponibles: Math.floor(Math.random() * 15)
        }
    }


    handleSimilares = (value) => {
        this.props.similares(value);
    }

    handleAdd = (id, price) => {
        if (this.state.disponibles > 0)
        {
            this.props.itemToAdd(id, price);
            this.setState({disponibles: this.state.disponibles - 1});
        }
        else
        {
            alert("No se pueden añadir más.");
        }       
    }
    render()
    {
        const product = this.props.product;
        const codeBlock = (
            <div className="stockItem col-sm-12 col-md-6 col-lg-4 ">
                <h3 className="productName">{product.name}</h3>
                <p className="prodId"><strong>Id Producto: </strong>{product.id}</p>
                <div className="sizePrice">
                    <p>{`${product.tam.x}x${product.tam.y}x${product.tam.z}`} cm</p>
                    <p>{product.price} €</p>
                </div>
                <div className="sizePrice">
                    <p>Disponibles: </p>
                    <p>{this.state.disponibles}</p>
                </div>
                <div className="itemOptions">
                    <button type="button" className="btn btn-secondary btn-sm" onClick={this.handleSimilares.bind(this, this.props.product.name)}>Similares</button>
                    <button type="button" className="btn btn-primary btn-sm" onClick={this.handleAdd.bind(this, this.props.keycode, product.price)}>Añadir</button>
                </div>
            </div>
        );
        
        // NINGUNA BÚSQUEDA
        /*
        * Utilizo expresiones de comparación no estrictas (mal)
        * Lo suyo sería hacer expresiones regulares
        */
        if (this.props.filter.length <= 0)
        {
            if (this.state.display) this.setState({display: false});
            return codeBlock;
        }
        else
        {
            let found = false;
            Object.entries(product).some ( prop => {
                if (!found)
                {
                    if (typeof prop[1] != "object")
                    {
                        if (prop[1] == this.props.filter) found = true;

                    }
                    else
                    {
                        // segunda capa de búsqueda
                        const s = prop[1];
                        const r = this.props.filter;
                        if (s.x == r || s.y == r || s.z == r) found = true;
                    }
                }
                else if (found)
                {
                    if (this.state.display === false) this.setState({display: true});
                    return true;
                }
            });
            
            if (this.state.display) return codeBlock;
            else return "";
        }

        
    }
}


export default Catalog;