import React, {Component, Fragment} from 'react';



class TotalPrice extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
            alertar: true
        }
    }

    handleClick = () => {
        alert('no');
    }
    render()
    {
        if (this.props.prices.length > 0)
        {
            let amount = 0;
            for (let i = 0; i < this.props.prices.length; i++)
            {
                amount += this.props.prices[i];
            }
            if (amount > 1500 && this.state.alertar) 
            {
                alert("wow, va a salir cara la compra");
                this.setState({alertar: false});
            }
            return (
                <Fragment>
                    <div className="col-md-4">TOTAL: </div>   
                    <div className="col-md-4 text-center">({this.props.prices.length} objetos) {amount} €</div>   
                    <div className="col-md-4 text-right"> <button onClick={this.handleClick.bind(this)} className="btn btn-sm btn-success">Proceder Compra</button> </div>   
                </Fragment>
            );
        }
        else return ("");
    }
}

export default TotalPrice;