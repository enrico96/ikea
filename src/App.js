import React from 'react';
import './App.css';


import Catalog from  './Lists/Catalog/Catalog';
import Purchase from './Lists/Purchase/Purchase';
import TotalPrice from './Lists/TotalPrice';
import Search from './Lists/Search';


class App extends React.Component
{
  
  constructor(props)
  {
    super(props);
    this.state = {
      list: [],
      prices: [],
      search: "",
      updateCode: "",
    };
  }

  parentSearchSim = (value) => {
    this.setState({search: value});
  }
  
  parentFunction = (data, price) => {
    this.setState({list: this.state.list.concat(data)});
    this.setState({prices: this.state.prices.concat(price)});
  }

  removePurchase = (id, code) => {
    // List slice
    const a = this.state.list.slice(0, id);
    const b = this.state.list.slice(id +  1, this.state.list.length);

    // Prices slice
    const c = this.state.prices.slice(0, id);
    const d = this.state.prices.slice(id +  1, this.state.prices.length);

    this.setState({list: a.concat(b)});
    this.setState({prices: c.concat(d)});
    this.setState({updateCode: code});

  }

  searchProducts = (value) => {
    this.setState({search: value});
  }

  render()
  {
    const containersStyle = {maxHeight:"400px", overflowY: "scroll"};
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-6 col-lg-6 col-sm-12">
            <h2>Catálogo de Compra</h2>
            <hr className="border"/>
            <div id="stockList" style={containersStyle} className="row">
              <Catalog updateCode={this.state.updateCode} handleSearchSim={this.parentSearchSim.bind(this)}  handleParentFunction={this.parentFunction.bind(this)} filter={this.state.search}/>
            </div>
          </div>
          <div  className="col-md-6 col-lg-6 col-sm-12">
            <h2>Lista de Compra</h2>
            <hr className="border"/>
            <div style={containersStyle} className="row">
              <Purchase 
                items={this.state.list} 
                handleDeletion={this.removePurchase.bind(this)}
              />
            </div>
          </div>
        </div>
        {/* Lista total de compra */}
        <div className="row d-flex justify-content-between totalPrice">
          <div className="col-md-6 col-sm-12"><Search handleSearch={this.searchProducts.bind(this)} value={this.state.search}/></div>
          <div className="col-md-6 col-sm-12 bg-dark text-light">
            <div className="row"><TotalPrice prices={this.state.prices} /></div>
          </div>

        </div>
      </div>
    );
  }
}

export default App;
